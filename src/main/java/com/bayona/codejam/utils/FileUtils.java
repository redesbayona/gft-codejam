package com.bayona.codejam.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlbf on 31/03/2017.
 */
public class FileUtils {

    public static final String UTF_8 = "UTF-8";

    public static List<String> readFile(final String file) {
        final List<String> strings = new ArrayList<String>();
        String line = null;
        try {
            final FileReader fileReader = new FileReader(file);
            final BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                strings.add(line);
            }
            bufferedReader.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return strings;
    }

    public static void writeFile(final String file,List<String> content) {

        try {
            PrintWriter writer = null;
            writer = new PrintWriter(file, UTF_8);
            for (String line : content){
                writer.println(line);
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
