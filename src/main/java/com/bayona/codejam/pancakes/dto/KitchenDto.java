package com.bayona.codejam.pancakes.dto;

/**
 * Created by mlbf on 11/04/2017.
 */
public class KitchenDto {

    private String pancakes;
    private int fork;
    private int flips;

    public String getPancakes() {
        return pancakes;
    }

    public void setPancakes(String pancakes) {
        this.pancakes = pancakes;
    }

    public int getFork() {
        return fork;
    }

    public void setFork(int fork) {
        this.fork = fork;
    }

    public int getFlips() {
        return flips;
    }

    public void setFlips(int flips) {
        this.flips = flips;
    }
}
