package com.bayona.codejam.pancakes.dto.enums;

/**
 * Created by mlbf on 11/04/2017.
 */
public enum CaseCode {

    SMALL("pancakes/A-small-practice.in", "pancakes/A-small-practice.out"),LARGE("pancakes/A-large.in","pancakes/A-large.out");

    private String input;
    private String output;

    CaseCode(String input, String output){
        this.input=input;
        this.output=output;
    }

    public String getInput() {
        return input;
    }

    public String getOutput() {
        return output;
    }
}
