package com.bayona.codejam.pancakes;

import com.bayona.codejam.pancakes.dto.enums.CaseCode;
import com.bayona.codejam.pancakes.services.PancakesService;
import com.bayona.codejam.pancakes.services.PancakesServiceImpl;
import com.bayona.codejam.utils.FileUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlbf on 08/04/2017.
 */
public class OversizedPancakeFlipper {

    private static PancakesService pancakesService = new PancakesServiceImpl();

    public static void main(String[] args) {

        CaseCode caseCode = CaseCode.SMALL;
        List<String> lines = FileUtils.readFile(OversizedPancakeFlipper.class.getClassLoader().getResource(caseCode.getInput()).getFile());
        List<String> solutions = new ArrayList<String>();

        for(int i = 1 ; i < Integer.valueOf(lines.get(0)) ; i++){

            String solution = manageKitchen(lines.get(i), i);
            solutions.add(solution);
        }

        FileUtils.writeFile(caseCode.getOutput(),solutions);

    }

    private static String manageKitchen(String line, int numberOfCase) {
        return null;
    }
}
