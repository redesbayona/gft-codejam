package com.bayona.codejam.pancakes.services;

import com.bayona.codejam.pancakes.dto.KitchenDto;

/**
 * Created by mlbf on 11/04/2017.
 */
public interface PancakesService {

    boolean isFinised(String pancakes);

    void flipPancakes(KitchenDto kitchen, int from);

    boolean isBlocked(int fork,String pancakes);

    int findFirstFlip(String pancakes);

    KitchenDto createKitchen(String line);

    String formatOutput(int numberOfCase, int moves,boolean possible);
}
