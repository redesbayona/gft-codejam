package com.bayona.codejam.pancakes.services;

import com.bayona.codejam.pancakes.dto.KitchenDto;

/**
 * Created by mlbf on 11/04/2017.
 */
public class PancakesServiceImpl implements PancakesService{

    public boolean isFinised(String pancakes) {
        return false;
    }

    public void flipPancakes(KitchenDto kitchen, int from) {

    }

    public boolean isBlocked(int fork, String pancakes) {
        return false;
    }

    public int findFirstFlip(String pancakes) {
        return 0;
    }

    public KitchenDto createKitchen(String line) {
        return null;
    }

    public String formatOutput(int numberOfCase, int moves, boolean possible) {
        return null;
    }
}
